var buttonEnviarFormulario = document.getElementById('form-submit');
var bloquearBoton = false;

buttonEnviarFormulario.onclick = () => {
    var name = document.getElementById('name');
    var email = document.getElementById('email');
    var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (!bloquearBoton && (name.value.length > 0 && email.value.match(mailformat))) {
        var request = new XMLHttpRequest();

        bloquearBoton = true;
        request.open('POST', 'https://api.sneakerstore.com.mx/api/registrar-landing', false);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify({
            "landing": 2,
            "name": name.value,
            "email": email.value,
        }));
        if (request.status == 200) {
            alert('¡Registro realizado con éxito!');
            name.value = '';
            email.value = '';
            bloquearBoton = false;
        } else {
            alert('¡Por favor vuelve a intentar registrarte!');
            bloquearBoton = false;
        }  
    } else {
        return;
    }
    
}